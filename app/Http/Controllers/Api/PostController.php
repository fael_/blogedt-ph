<?php

namespace App\Http\Controllers\Api;
use App\Models\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::all();

        return response($posts, 201);
    }

    public function show($id)
    {
        $post = Post::findOrFail($id);

        return $post;
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'string',
            'content' => 'string'
        ]);

        $newPost = new Post;

        $newPost->title = $request->title;
        $newPost->content = $request->content;

        $newPost->save();

        return Post::all();
    }

    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        $post->delete();

        return response(Post::all(), 200);
    }


    public function update(Request $request, $id)
    {
        // 1. VALIDATE!
        // 2. get single post
        // 3. update single post
        // 4. save changes to single post
        // 5. return updated post as a response

        $request->validate([
            'title' => 'string',
            'content' => 'string'
        ]);

        $post = Post::findOrFail($id);
        
        $post->title = $request->title;
        $post->content = $request->content;

        $post->save();

        return response($post, 200);
    }
}
