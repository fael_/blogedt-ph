require('./bootstrap');

import Vue from 'vue';
import router from './routes';
import VueRouter from 'vue-router';
import Alert from './components/globals/Alert.vue';
import Index from './Index.vue';

window.Vue = Vue;

Vue.component('Alert', Alert);
Vue.use(VueRouter);

const app = new Vue({
    el: '#app',
    router: router,
    components: {
        'Index' : Index
    }
});
