import VueRouter from 'vue-router';
import BlogPostList from '../js/components/BlogPostList.vue';
import BlogPost from './components/children/BlogPost.vue';
import About from './components/About.vue';

const routes = [
    {
        path: '/',
        component: BlogPostList,
        name: 'blog-posts'
    },
    {
        path: '/about',
        component: About,
        name: 'about'
    },
    {
        path: '/posts/:id',
        component: BlogPost,
        name: 'blog-post'
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router;

