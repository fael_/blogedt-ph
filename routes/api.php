<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('posts', 'App\Http\Controllers\Api\PostController@index');
Route::get('posts/{id}', 'App\Http\Controllers\Api\PostController@show');
Route::patch('posts/{id}/update', 'App\Http\Controllers\Api\PostController@update');
Route::post('posts/store', 'App\Http\Controllers\Api\PostController@store');
Route::delete('posts/{id}/destroy', 'App\Http\Controllers\Api\PostController@destroy');
